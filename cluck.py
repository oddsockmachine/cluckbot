import logging
logging.basicConfig(filename='cluckbot.log', level=logging.DEBUG)
import os
from slack import WebClient
from slack.errors import SlackApiError
from random import choice
from datetime import datetime
from sys import argv

logging.debug("\n\n---------\n")

# Expect either morning or evening as command line arg
action = os.environ["ACTION"]
if action not in ["morning", "evening"]:
  logging.debug("Unknown action, exiting")
  exit(1)
logging.debug(f"Action is {action}")

# Setup Slack
slack_token = os.environ["SLACK_API_TOKEN"]
client = WebClient(token=slack_token)
logging.debug(client)

# What day is it, what are we doing, who's responsible?
today = datetime.now()
logging.debug(f"Today is {today}")
day_number = today.weekday()
if action == "evening":
  day_number = (day_number + 1) % 7
logging.debug(f"Day number is {day_number}")
schedule = {
    0: "Batu",   # M
    1: "Caleb",    # T
    2: "Mase",  # W
    3: "Sarah",  # T
    4: "Sam",  # F
    5: "Aya",  # S
    6: "Alice",   # S
}

todays_person = schedule[day_number]
logging.debug(f"Today's person is {todays_person}")

# Get user IDs
user_IDs = {
    'Sarah':  'UDS0JAP42',
    'Aya':    'U02FCGY78GP',
    'Mase':   'UDRGQUZ9N',
    'David':  'ULXRGRQNS',
    'Batu':   'U01S5TCS1JQ',
#    'Batu':   '<!channel>, Batu is out',
    'Caleb':  'U02GF1X6FC2',
#    'Caleb': ' <!channel> , Caleb is out',
    'Alice':  'UDS7GJN69',
 #   'Alice': '<!channel>, Alice is out',
    'Dave':   'U0106EQRKKP',
    'Sam':    'U02QE93U63Y'
}

chickens = ['Precita', 'Marina', 'Crissy']
emojis = ['hatched_chick', 'chicken', 'baby_chick', 'hatching_chick']
commands = ["it's your turn to look after me!",
            "come look at this big poop I did!",
            "we get to hang out today!",
            "got any mealworms?",
            "you're my best friend (also come clean up after me)",
            "bok bok buh-gawk!",
            "Cheep cheep cheep cheep beep cheep",
            "I just made a perfect :poop:! Come see it?",
            "Come see if I tipped over the water and flooded my house... again",
            "Let me out, I've got things to peck",
            "It's a new day and I want to be free!",
            "Are you awake yet? Want some eggs?"]

todays_chick = choice(chickens)
todays_emoji = choice(emojis)
todays_command = choice(commands)
todays_userID = user_IDs[todays_person]

reminder = f"Hi {todays_person}, quick reminder that you're on chicken duty tomorrow morning!"
message = f":{todays_emoji}:{todays_chick}:{todays_emoji}: says:\n'Hey <@{todays_userID}>, {todays_command}'" if action == "morning" else reminder
logging.debug(f"Message is: {message}")
channel = "gardenandchickenz" if action == "morning" else todays_userID
logging.debug(f"Channel is {channel}")
# Post to Slack
try:
  response = client.chat_postMessage(
    channel=channel,
    text=message
  )

  #response = client.chat_postMessage(
   # channel=user_IDs['Dave'],
   # text=message
  #)

except SlackApiError as e:
  # You will get a SlackApiError if "ok" is False
  assert e.response["error"]  # str like 'invalid_auth', 'channel_not_found'
logging.debug("Done!")
